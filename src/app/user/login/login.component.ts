import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide: boolean = true;
  isError: boolean = false;
  loginForm: FormGroup;
  returnUrl: string;
  isLoading: boolean = false;
  loginResponse: any;

  constructor(public lf: FormBuilder, public router: Router, public route: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    localStorage.clear();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    this.loginForm = this.lf.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login() {
    this.isLoading = true;
    this.isError=false;
    let obj = {};
    let username = this.loginForm.value['username'];
    let password = this.loginForm.value['password'];
    obj = {
      "userDetails": [{
        "userName": username,
        "password": password
      }]
    }
    this.userService.login(obj).subscribe(
      (res) => {
        debugger;
        this.isLoading = false;
        this.loginResponse = res;
        localStorage.setItem('token', this.loginResponse.token);
        let authTokenDecypt = this.parseJwt(this.loginResponse.token);
        debugger;
        if(authTokenDecypt.permissionList.role.toLowerCase()=="arc"){
          localStorage.setItem('currentUser',JSON.stringify(authTokenDecypt.permissionList));
          this.router.navigate([this.returnUrl]);
        }
      else{
        this.isLoading = false;
        this.isError=true;
      }
      },
      (err) => {
        this.isLoading = false;
        this.isError=true;
      }
    )
  }

  parseJwt(token) {
    
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  };
}
