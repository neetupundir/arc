import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { AnalyticService } from '../analytic.service';

declare var Packery: any;
declare var Draggabilly: any;

@Component({
  selector: 'app-analytic',
  templateUrl: './analytic.component.html',
  styleUrls: ['./analytic.component.scss']
})
export class AnalyticComponent implements OnInit {

  constructor(private analyticService: AnalyticService, private cd: ChangeDetectorRef) { }

  draggableElems: any;
  response: any;
  elem: any;
  pckry: any;
  draggies: Array<any> = [];
  modelList: any;
  mobileBrandResponse: any;
  cityResponse: any;
  isLoading: boolean = false;
  startDate: any;
  endDate: any;
  brandValue: any;
  modelValue: any;
  locationValue: any;
  isSearched: boolean = false;
  filtersObj: any;
  onLoadRequestObj = {
    "currOrderState": "",
    "brandIds": [],
    "modelIds": [],
    "stateIds": [],
    "fromDate": "",
    "toDate": ""
  }
  /*declation of input parameter for dashboard items*/
  @Input() public show: boolean;

  @Output()
  changeExpansion: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**Declearation for show hide panel content */
  isToggle: boolean = true;
  isToggle1: boolean = true;

  ngOnInit() {
    this.endDate = new Date();
    this.startDate = new Date();
    this.startDate.setDate(this.endDate.getDate() - 20);
    this.getBrandNames();
    this.getLocation();
  }

  setFilter() {
    
    let brandArray = [];
    let modelArray = [];
    let locationArray = [];
    for (let i = 0; i < this.brandValue.length; i++) {
      brandArray.push(this.brandValue[i].deviceBrandId);
    }
    for (let i = 0; i < this.modelValue.length; i++) {
      modelArray.push(this.modelValue[i].deviceModelId);
    }
    for (let i = 0; i < this.locationValue.length; i++) {
      locationArray.push(this.locationValue[i].cityMasterId);
    }
    this.onLoadRequestObj.brandIds = brandArray;
    this.onLoadRequestObj.modelIds = modelArray;
    this.onLoadRequestObj.stateIds = locationArray;
    this.onLoadRequestObj.fromDate = this.convertDate(this.startDate);
    this.onLoadRequestObj.toDate = this.convertDate(this.endDate);
    this.filtersObj = JSON.parse(JSON.stringify(this.onLoadRequestObj));

  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
  }

  getLocation() {
    var obj = {
      "countryId": 1
    }

    this.analyticService.getCityLocation(obj).subscribe(
      (res) => {

        this.cityResponse = res.responseObj;
      },
      (err) => {

      }
    )
  }

  getBrandModel(brands) {
    this.modelValue = '';
    let brandId = [];
    if (typeof brands != "undefined") {
      for (let i = 0; i < brands.length; i++) {
        brandId.push(brands[i].deviceBrandId)
      }
      let obj = {
        "bandIds": brandId,
        "countryId": 1
      }
      this.isLoading = true
      this.analyticService.getBrandModelList(obj).subscribe(
        (res) => {
          this.isLoading = false
          this.modelList = res.responseObj;
        }
      )
    }
  }

  getBrandNames() {
    var obj = {
      "countryId": 1
    }

    this.analyticService.getMobileBrandName(obj).subscribe(
      (res) => {

        this.mobileBrandResponse = res.responseObj;
      }
    )
  }

  disableFilter() {
    if ((typeof this.brandValue == 'undefined' || this.brandValue.length < 1) ||
      (typeof this.modelValue == 'undefined' || this.modelValue.length < 1) ||
      (typeof this.locationValue == 'undefined' || this.locationValue.length < 1) ||
      (typeof this.modelValue == 'undefined' || this.modelValue.length < 1) ||
      (typeof this.startDate == 'undefined' || !this.startDate) ||
      (typeof this.endDate == 'undefined' || !this.endDate)) {
      return true;
    }
    else {
      return false;
    }
  }
  ngAfterViewInit() {
    this.draggableElems = document.querySelectorAll('.grid-item');
    this.elem = document.querySelector('.grid');
    this.pckry = [];
    this.draggable();
  }



  draggable() {
    for (let i = 0, len = this.draggableElems.length; i < len; i++) {
      let draggableElem = this.draggableElems[i];
      let draggie = new Draggabilly(draggableElem, {
        containment: '.grid',
        handle: '.handle'
      });
      let pckry = new Packery(this.elem, {
        itemSelector: '.grid-item',
        gutter: 0
      });
      pckry.bindDraggabillyEvents(draggie);
      this.pckry.push(pckry);
      this.draggies.push(draggie);
    }
  }
}