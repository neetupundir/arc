import { Component, OnInit, Input, Output, EventEmitter, trigger, ChangeDetectorRef, style, transition, animate } from '@angular/core';
import { EstimatedDetailComponent } from '../estimated-detail/estimated-detail.component';
import { MatDialog } from '@angular/material';
declare var Chart: any;
import { AnalyticService } from '../analytic.service';
@Component({
  selector: 'app-estimated-tat',
  templateUrl: './estimated-tat.component.html',
  styleUrls: ['./estimated-tat.component.scss'],
  animations: [
    trigger(
      'isToggle', [
        transition(':enter', [
          style({ 'height': '0', 'opacity': '0' }),
          animate('200ms linear', style({ 'height': '*', 'opacity': '1' }))
        ]),
        transition(':leave', [
          style({ 'height': '*', 'opacity': '1' }),
          animate('200ms linear', style({ 'height': '0', 'opacity': '0' })
          )])
      ]
    )
  ]
})
export class EstimatedTatComponent implements OnInit {

  estimatedCalRes: any;
  onLoadRequestObj = {
    "currOrderState": "Pending for Approval",
    "brandIds": [],
    "modelIds": [],
    "stateIds": [],
    "fromDate": null,
    "toDate": null
  }
  noData: boolean = false;
  hideWidgetLoader: boolean = false;

  @Input() public set estFilter(val: any) {
    if (typeof val != "undefined") {
      this.onLoadRequestObj = val;
      this.onLoadRequestObj.currOrderState = "Pending for Approval",
        this.getEstimatedTATCalculation();
    }
  }

  /*declation of input parameter for dashboard items*/
  @Input() public show: boolean;

  @Output()
  changeExpansion: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**Declearation for show hide panel content */
  isToggle: boolean = true;

  // Pie
  public pieChartLabels: string[] = [];
  public pieChartData: number[] = [];
  public pieChartType: string = 'pie';
  public pieChartColors: any[] = [{ backgroundColor: ["#64a70b", "#ff821f", "#002d72", "#888b8d", "#ffe200", "#7c00ff","#f44336","#b4f436","#36f4e2","#e036f4","#212123","#8cb1d8"] }]
  constructor(public dialog: MatDialog, private analyticService: AnalyticService) { }

  ngOnInit() {
    Chart.defaults.global.legend.position = 'right';
    this.getEstimatedTATCalculation();
  }

  // events
  public chartClicked(e: any): void {

  }

  public chartHovered(e: any): void {

  }


	/**
	 *Method for toggle show hide
	*/
  toggle() {
    this.isToggle = !this.isToggle;
    this.changeExpansion.emit(this.isToggle);
  }


  getEstimatedTATCalculation() {
    this.noData = true;
    this.pieChartData=[];
    this.pieChartLabels=[];
    this.analyticService.getEstimatedTATCalculation(this.onLoadRequestObj).subscribe(
      (res) => {   
        if (res.responseCode == 200) {
          this.noData = false;
          this.hideWidgetLoader = true;
          this.estimatedCalRes = res.responseObj;
          for (let key in this.estimatedCalRes.detailsListMap) {
            this.pieChartData.push(parseInt(this.estimatedCalRes.detailsListMap[key]));
            this.pieChartLabels.push(key);
          }
        }
        else {
          this.noData = true;
          this.hideWidgetLoader = true;
        }
      },
      (err) => {
        this.noData = true;
      }
    )
  }


  estimatedDetail(): void {
    let dialogRef = this.dialog.open(EstimatedDetailComponent, {
      width: '1100px',
      disableClose: false,
      data: { obj: this.onLoadRequestObj }
    });
  }
}