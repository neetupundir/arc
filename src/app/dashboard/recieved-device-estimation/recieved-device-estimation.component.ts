import { Component, Inject, OnInit, ViewChild, ElementRef, Renderer2, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddSparePartComponent } from '../add-spare-part/add-spare-part.component';
import { PendingEstimationService } from '../service/pending-estimation.service';
import { DeviceInTransitService } from '../service/device-in-transit.service';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';
import { environment } from '../../../environments/environment'; 
import {MessageService} from '../service/message.service';
@Component({
  selector: 'app-recieved-device-estimation',
  templateUrl: './recieved-device-estimation.component.html',
  styleUrls: ['./recieved-device-estimation.component.scss']
})
export class RecievedDeviceEstimationComponent implements OnInit {
  BOMResponse: any;
  taxVal: number = 0;
  totalAmount: number = 0;
  serviceCharge: any;
  taxPercentage: any;
  addRecord: boolean = false;
  newPartName: string;
  newDescription: string;
  newPrice: any;
  newTaxes: any;
  newComment: string;
  addArray = [];
  newPartCode: string;
  submitBOMObj = {}
  res: any;
  comment: any;
  estimateArray: any = [];
  submitEstimateArray = [];
  taxGroup: any;
  countryId: any;
  modelId: any;
  brandId: any;
  taxgrpId: any;
  bomSubmitRes: any;
  bomRessMessage: any;
  isLoading: boolean = false;
  selectedArray = [];
  itemExist: boolean = true;
  amountCalculated: boolean = false;
  baseUrl: string = environment.hostUrl;
  showFileUploadSuccessMessage: any;
  showFileUploadSuccess: boolean = false;
  fileType: any;
  orderImage:any;
  orderImageRes:any;

  //ngx options
  options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  comments: string;
  //ngx options


  constructor(private messageService: MessageService,
    public dialog: MatDialog, private _renderer: Renderer2,
    public dialogRef: MatDialogRef<RecievedDeviceEstimationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private deviceInTransitService: DeviceInTransitService, private pendingEstimationService: PendingEstimationService
  ) {
    this.options = { concurrency: 1 };
    this.files = []; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.humanizeBytes = humanizeBytes;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  cancel() {
    this.addRecord = false;
    this.newPartName = '';
    this.newDescription = '';
    this.newPrice = '';
    this.newTaxes = '';
    this.newComment = '';
    this.newPartCode = '';
  }
  filesArray = [
    { viewValue: 'Bottom Image' },
    { viewValue: 'Front Image' },
    { viewValue: 'Back Image' },
    { viewValue: 'Left Image' },
    { viewValue: 'Right Image' }
  ];
  /**
   * Add spare part & price
   */
  addSharePart(): void {
    let dialogRef = this.dialog.open(AddSparePartComponent, {
      width: '280px',
      disableClose: true,
      data: { name: 'test' }
    });
  }

  onlyNumber(e) {

    if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return true;
    } else if (!(e.keyCode > 47 && e.keyCode < 58) || e.shiftKey) {
      e.preventDefault();
      return false;
    }
  }
  calPerValue(val, per) {
    var value = parseFloat(val);
    var percentage = parseFloat(per);
    return (value * percentage) / 100;
  }

  getBOM() {
    var obj = {
      "countryId": 1,
      "brandId": 1,
      "modelId": "1"
    }

    this.isLoading = true;
    this.pendingEstimationService.getBOM(obj).subscribe(
      (res) => {
        this.isLoading = false;
        this.BOMResponse = res;
        if (this.BOMResponse.response.bomTemplate.length > 0) {
          this.countryId = this.BOMResponse.response.bomTemplate[0].countryId;
          this.modelId = this.BOMResponse.response.bomTemplate[0].modelId;
          this.brandId = this.BOMResponse.response.bomTemplate[0].brandId;
        }
      },
      (err) => {

      }
    )
  }

  add() {
    debugger;
    let obj = {
      "countryId": this.countryId,
      "modelId": this.modelId,
      "brandId": this.brandId,
      'partName': this.newPartName,
      'partDesc': this.newDescription,
      'partCode': this.newPartCode,
      'price': this.newPrice,
      "taxGrpId": this.newTaxes
    }
    this.pendingEstimationService.addDevicesForAMS(obj).subscribe(
      (res) => {
        this.addRecord = false;
        this.getBOM();
        this.cancel();
      },
      (err) => {

      }
    )
  }


  enableAdd() {
    if (!this.newPartName || !this.newPartCode || !this.newDescription || !this.newPrice || typeof (this.newTaxes) == 'undefined') {
      return true
    }
    else {
      return false;
    }
  }

  calAmountEstimation(serCharge, taxPercentage) {
    if (serCharge && taxPercentage) {
      let taxVal = (parseFloat(serCharge) * parseFloat(taxPercentage)) / 100;
      return taxVal;
    }
  }

  ngOnInit() {
    this.getBOM();
    this.getRepairOrderDetails();
    this.getTaxGroup();
    this.getOrderImages();
  }


  getOrderImages() {
    debugger;
    let obj = {
      "orderId": this.dialogRef._containerInstance._config.data.orderId
    }
    this.pendingEstimationService.getOrderImages(obj).subscribe(
      (res) => {
        this.orderImageRes=res.responseObj;
      },
      (err) => {

      }
    )
  }

  getTaxGroup() {
    let obj = {
      "countryId": 1
    }
    this.pendingEstimationService.getTaxGroup(obj).subscribe(
      (res) => {

        this.taxGroup = res;
      },
      (err) => {

      }
    )
  }
  getRepairOrderDetails() {
    this.isLoading = true;
    var obj = {
      "orderId": this.dialogRef._containerInstance._config.data.orderId
    }

    this.deviceInTransitService.getRODetails(obj).subscribe(
      (res) => {

        this.isLoading = false;
        this.res = res;
      },
      (err) => {

      }
    )
  }



  submitBOM() {

    this.isLoading = true;
    this.initializeEstimateArray();
    for (let i = 0; i < this.taxGroup.responseObj.length; i++) {
      if (this.taxGroup.responseObj[i].rateApplicable == this.taxPercentage) {
        this.taxgrpId = this.taxGroup.responseObj[i].taxGroupId;
        break;
      }
    }
    this.submitBOMObj = {
      "orderId": this.dialogRef._containerInstance._config.data.orderId,
      "totalAmt": this.totalAmount,
      "approvedComments": this.comment,
      "action": "Add New Estimate",
      "status": true,
      "serviceCharge": this.serviceCharge,
      "taxGrpId": this.taxgrpId,
      "taxValue": this.taxVal,
      "orderEstimateDtls": this.initializeEstimateArray()
    }

    this.pendingEstimationService.saveEstimation(this.submitBOMObj).subscribe(
      (res) => {
        debugger;
        this.bomSubmitRes = res;
        this.bomRessMessage = this.bomSubmitRes.response.Estimation;
        this.isLoading = false;
        this.messageService.sendMessage('reloadFromPE');   
        this.onNoClick();       
      },
      (err) => {

      }
    )
  }

  initializeEstimateArray() {
    this.estimateArray = [];
    for (let i = 0; i < this.BOMResponse.response.bomTemplate.length; i++) {
      if (this.BOMResponse.response.bomTemplate[i].checked) {
        this.estimateArray.push(
          {
            "partName": this.BOMResponse.response.bomTemplate[i].partName,
            "partId": this.BOMResponse.response.bomTemplate[i].partId,
            "partCode": this.BOMResponse.response.bomTemplate[i].partCode,
            "description": this.BOMResponse.response.bomTemplate[i].partDesc,
            "comments": this.BOMResponse.response.bomTemplate[i].comments,
            "partTaxValue": this.BOMResponse.response.bomTemplate[i].taxValue,
            "partTaxAmt": this.calAmountEstimation(this.BOMResponse.response.bomTemplate[i].price, this.BOMResponse.response.bomTemplate[i].taxValue),
            "approvalStatus": 2,
            "approvalComments": "Approved by ARC",
            "price": this.BOMResponse.response.bomTemplate[i].price
          }
        );
      }
    }
    return this.estimateArray;
  }



  calculateTaxAndAmount() {
    this.taxVal = 0;
    this.totalAmount = 0;
    for (var i = 0; i < this.BOMResponse.response.bomTemplate.length; i++) {
      if (this.BOMResponse.response.bomTemplate[i].checked) {
        var taxVal = (parseFloat(this.BOMResponse.response.bomTemplate[i].price) * parseFloat(this.BOMResponse.response.bomTemplate[i].taxValue)) / 100;
        this.taxVal += taxVal;
        this.totalAmount += parseFloat(this.BOMResponse.response.bomTemplate[i].price);
        this.BOMResponse.response.bomTemplate[i].taxamt = taxVal;
        this.amountCalculated = true;
      }
    }
    if (typeof this.serviceCharge == 'undefined') {
      this.serviceCharge = 0;
    }
    if (typeof this.serviceCharge != 'undefined' && typeof this.taxPercentage == 'undefined') {
      this.taxPercentage = 0;
    }
    this.taxVal += (parseFloat(this.serviceCharge) * parseFloat(this.taxPercentage)) / 100;
    this.taxVal = parseFloat(this.taxVal.toFixed(2));
    this.totalAmount += parseFloat(this.serviceCharge) + this.taxVal;
    this.totalAmount = parseFloat(this.totalAmount.toFixed(2));
  }

  calculateValues(item) {
    let value = (item.price * item.taxValue) / 100;
    var valueParsed = parseFloat(value.toFixed(2));
    return valueParsed;
  }

  startUpload(): void {
    this.files=[];
    const event: UploadInput = {
      type: 'uploadAll',
      url: this.baseUrl + '/repairPortal/uploadDeviceImages',
      method: 'POST',
      headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') },
      data: { orderId: this.dialogRef._containerInstance._config.data.orderId, fileType: this.fileType }
    };  
    this.uploadInput.emit(event);
  }

  onUploadOutput(output: UploadOutput): void {

    this.files.push(output.file);

    if (output.type === 'start') {
      this.isLoading = true;
    }
    if (output.type === 'done') {
      if (output.file.response.responseCode == '200') {
        this.getOrderImages();
        var uploadButton = document.getElementById("uploadButton");
        uploadButton.click();
        setTimeout(() => {    //<<<---    using ()=> syntax
          document.getElementById('showFileUploadSuccess').innerHTML = output.file.response.responseObj;
          document.getElementById('showFileUploadSuccess').style.display = 'block';
          document.getElementById('myLoader').style.display = 'none';
        }, 4000);      
       
        this.resetItems();
        setTimeout(() => {
          document.getElementById('showFileUploadSuccess').style.display = 'none';
        }, 5000);

      }
      else {
        this.isLoading = false;
      }
    }
  }

  resetItems(){
    this.fileType='';
    this.orderImage='';
  }

}

