import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { PendingEstimationService } from '../service/pending-estimation.service';
import { Subject } from 'rxjs';
import { RepairOrderDetailsComponent } from '../repair-order-details/repair-order-details.component';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { RecievedDeviceEstimationComponent } from '../recieved-device-estimation/recieved-device-estimation.component'
import { DeviceInTransitDisputeComponent } from '../device-in-transit-dispute/device-in-transit-dispute.component';

@Component({
  selector: 'app-zoom-pending-estimation',
  templateUrl: './zoom-pending-estimation.component.html',
  styleUrls: ['./zoom-pending-estimation.component.scss']
})
export class ZoomPendingEstimationComponent {

  uploadInput : any;
	options : any = {};
  deviceResponse:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  isTableData:boolean=false;
  
 	constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ZoomPendingEstimationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private pendingEstimationService:PendingEstimationService  ) { }

  getPendingEstimation() {
    let currentUser=localStorage.getItem('currentUser');
    var obj = {
      "arcId": JSON.parse(currentUser).arcId,
      "country": "India",
      "orderState": "Pending Estimation",
      "isDetailView":"true"
    }
    
    this.pendingEstimationService.getPendingEstimation(obj).subscribe(
      (res)=>{

        this.deviceResponse = res.response.roList;
        this.dtTrigger.next();
        this.isTableData = true;
      },
      (err) => {

      }
    )
  }

  ngAfterViewInit() {
    this.dtTrigger.next();
 }

 ngOnInit() {
  this.getPendingEstimation();
}

  onNoClick(): void {
    this.dialogRef.close();
  }
  estimation(orderId): void {
    let dialogRef = this.dialog.open(RecievedDeviceEstimationComponent, {
      width: '700px',
      disableClose: true,
      data: { orderId: orderId }
    });
  }

  disputeDevice(orderId): void {
    let dialogRef = this.dialog.open(DeviceInTransitDisputeComponent, {
      width: '500px',
      disableClose: true,
      data: { orderId: orderId }
    });
  }

  downloadData(): void{
    let responseArr = this.deviceResponse, internalArr = [],externalArr = [];

    for(let result of responseArr){
      internalArr=[];
      internalArr.push(result.brand);
      internalArr.push("-"+result.imeiNo+"-");
      internalArr.push(result.receivedDateTime);
      internalArr.push(result.estimationDueDate);
      externalArr.push(internalArr);
    }
    var options = { 
      headers: ['Brand Name','IMEI No.','Received Date Time','Estimation Due Date']
      };
      new Angular2Csv(externalArr, 'Pending Estimation Report',options);    
      
  }
  getRepairOrderDetails(orderId): void {
    let dialogRef = this.dialog.open(RepairOrderDetailsComponent, {
      width: '500px',
      data: { orderID: orderId }
    });
  }

}
