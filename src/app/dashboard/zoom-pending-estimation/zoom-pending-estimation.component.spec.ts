import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomPendingEstimationComponent } from './zoom-pending-estimation.component';

describe('ZoomPendingEstimationComponent', () => {
  let component: ZoomPendingEstimationComponent;
  let fixture: ComponentFixture<ZoomPendingEstimationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoomPendingEstimationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomPendingEstimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
