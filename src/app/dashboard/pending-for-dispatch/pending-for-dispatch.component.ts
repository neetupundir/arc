import { Component, OnInit, Input, Output, EventEmitter, trigger, style, transition, animate } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RepairOrderDetailsComponent } from '../repair-order-details/repair-order-details.component';
import { PendingForDispatchDetailComponent } from '../pending-for-dispatch-detail/pending-for-dispatch-detail.component';
import { PendingForDispatchService } from '../service/pending-for-dispatch.service';
import { ZoomPendingForDispatchComponent } from '../zoom-pending-for-dispatch/zoom-pending-for-dispatch.component';

@Component({
  selector: 'app-pending-for-dispatch',
  templateUrl: './pending-for-dispatch.component.html',
  styleUrls: ['./pending-for-dispatch.component.scss'],
  animations: [
    trigger(
      'isToggle', [
        transition(':enter', [
          style({ 'height': '0', 'opacity': '0' }),
          animate('200ms linear', style({ 'height': '*', 'opacity': '1' }))
        ]),
        transition(':leave', [
          style({ 'height': '*', 'opacity': '1' }),
          animate('200ms linear', style({ 'height': '0', 'opacity': '0' })
          )])
      ]
    )
  ]
})
export class PendingForDispatchComponent implements OnInit {

  /*declation of input parameter for dashboard items*/
  @Input() public show: boolean;

  @Input() public set changeInPendingForDispatch(val: any) {
		if (typeof val != "undefined") {
      debugger;
			this.ngOnInit();
		}
	}


  @Output()
  changeExpansion: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**Declearation for show hide panel content */
  isToggle: boolean = true;
  deviceResponse: any;
  res:any;

  // data = [
  //   { deviceName: 'Samsung 58', imeiNumber: '412438', status: 'BER' },
  //   { deviceName: 'Nokia 6', imeiNumber: '412538', status: 'Repaired' },
  //   { deviceName: 'Nokia NX', imeiNumber: '412538', status: 'RTO' }
  // ];

  constructor(public dialog: MatDialog, private pendingForDispatchService: PendingForDispatchService) { }

  ngOnInit() {
    this.getPendingForDispatch();
  }

  getPendingForDispatch() {
    let currentUser=localStorage.getItem('currentUser');
    var obj = {
      "arcId": JSON.parse(currentUser).arcId,
      "orderState": "Pending for Dispatch",
      "country": "India"
    }
    this.pendingForDispatchService.getPendingForDispatch(obj).subscribe(
      (res) => {
        debugger;
        this.deviceResponse = res.response.roList;
        this.isToggle = !this.isToggle;
        this.changeExpansion.emit(this.isToggle);
        setTimeout(() => {    //<<<---    using ()=> syntax
          this.isToggle = true;
          this.changeExpansion.emit(this.isToggle);
        }, 100);
      },
      (err) => {

      }
    )

  }
  /**
  *Method for toggle show hide
  */
  toggle() {
    this.isToggle = !this.isToggle;
    this.changeExpansion.emit(this.isToggle);
  }

  dispatch(id): void {
    let dialogRef = this.dialog.open(PendingForDispatchDetailComponent, {
      width: '500px',
      disableClose: true,
      data: { orderId:id }
    });
  }

  getRepairOrderDetails(orderId): void {
    let dialogRef = this.dialog.open(RepairOrderDetailsComponent, {
      width: '500px',
      data: { orderID: orderId }
    });
  }
  ZoomTransit():void{
    let dialogRef = this.dialog.open(ZoomPendingForDispatchComponent, {
      width: '1100px',
      disableClose: true,
      data: { name: 'test' }
    });
  }

}
