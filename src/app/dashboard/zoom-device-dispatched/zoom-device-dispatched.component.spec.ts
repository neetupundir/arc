import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomDeviceDispatchedComponent } from './zoom-device-dispatched.component';

describe('ZoomDeviceDispatchedComponent', () => {
  let component: ZoomDeviceDispatchedComponent;
  let fixture: ComponentFixture<ZoomDeviceDispatchedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoomDeviceDispatchedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomDeviceDispatchedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
