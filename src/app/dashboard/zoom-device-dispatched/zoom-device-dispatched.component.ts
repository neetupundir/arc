import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DeviceDispatchedService } from '../service/device-dispatched.service';
import { Subject } from 'rxjs';
import { RepairOrderDetailsComponent } from '../repair-order-details/repair-order-details.component';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { RecievedDeviceEstimationComponent } from '../recieved-device-estimation/recieved-device-estimation.component'
import { DeviceInTransitDisputeComponent } from '../device-in-transit-dispute/device-in-transit-dispute.component';

@Component({
  selector: 'app-zoom-device-dispatched',
  templateUrl: './zoom-device-dispatched.component.html',
  styleUrls: ['./zoom-device-dispatched.component.scss']
})
export class ZoomDeviceDispatchedComponent {

  uploadInput : any;
	options : any = {};
  deviceResponse:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  isTableData:boolean=false;
  
 	constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ZoomDeviceDispatchedComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private deviceDispatchedService:DeviceDispatchedService  ) { }

  getDeviceDispatched() {
    let currentUser=localStorage.getItem('currentUser');
    var obj = {
      "arcId":JSON.parse(currentUser).arcId,
      "country": "India",
      "orderState": "Device Dispatched",
      "isDetailView":"true"
    }
    
    this.deviceDispatchedService.getDeviceDispatched(obj).subscribe(
      (res)=>{
        this.deviceResponse = res;
        this.dtTrigger.next();
        this.isTableData = true;
      },
      (err) => {

      }
    )
  }

  ngAfterViewInit() {
    this.dtTrigger.next();
 }

 ngOnInit() {
  this.getDeviceDispatched();
}

  onNoClick(): void {
    this.dialogRef.close();
  }
  estimation(orderId): void {
    let dialogRef = this.dialog.open(RecievedDeviceEstimationComponent, {
      width: '700px',
      disableClose: true,
      data: { orderId: orderId }
    });
  }

  disputeDevice(orderId): void {
    let dialogRef = this.dialog.open(DeviceInTransitDisputeComponent, {
      width: '500px',
      disableClose: true,
      data: { orderId: orderId }
    });
  }

  downloadData(): void{
    let responseArr = this.deviceResponse, internalArr = [],externalArr = [];

    for(let result of responseArr){
      internalArr=[];
      internalArr.push(result.brand);
      internalArr.push("-"+result.imeiNo+"-");
      internalArr.push(result.receivedDateTime);
      internalArr.push(result.estimationDueDate);
      externalArr.push(internalArr);
    }
    var options = { 
      headers: ['Brand Name','IMEI No.','Received Date Time','Estimation Due Date']
      };
      new Angular2Csv(externalArr, 'Pending Estimation Report',options);    
      
  }
  getRepairOrderDetails(orderId): void {
    let dialogRef = this.dialog.open(RepairOrderDetailsComponent, {
      width: '500px',
      data: { orderID: orderId }
    });
  }

}
