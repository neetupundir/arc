import { Component, Inject, EventEmitter, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DeviceInTransitService } from '../service/device-in-transit.service';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';
import { environment } from '../../../environments/environment';
import { MessageService } from '../service/message.service';
@Component({
  selector: 'app-device-in-transit-dispute',
  templateUrl: './device-in-transit-dispute.component.html',
  styleUrls: ['./device-in-transit-dispute.component.scss']
})
export class DeviceInTransitDisputeComponent implements OnInit {

  pickList: any;
  showWrongExtError: boolean = false;
  comments: any;
  disputeTypes: any;
  baseUrl: string = environment.hostUrl;
  showFileUploadSuccess: boolean = false;
  showFileUploadSuccessMessage: string;
  isLoading: boolean = false;

  //ngx options
  options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  //ngx options

  constructor(private messageService: MessageService, public dialogRef: MatDialogRef<DeviceInTransitDisputeComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private deviceInTransitService: DeviceInTransitService) {
    this.options = { concurrency: 1 };
    this.files = []; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.humanizeBytes = humanizeBytes;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  startUpload(): void {
    this.isLoading = true;
    const event: UploadInput = {
      type: 'uploadAll',
      url: this.baseUrl + '/repairPortal/saveDispute',
      method: 'POST',
      headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') },
      data: { comment: this.comments, disputeTypes: this.disputeTypes, orderId: this.dialogRef._containerInstance._config.data.orderId }
    };
    this.uploadInput.emit(event);
  }

  onUploadOutput(output: UploadOutput): void {

    this.files.push(output.file);
    if (output.type === 'done') {
      if (output.file.response.responseCode == '201') {
        this.isLoading = false;
        this.showFileUploadSuccessMessage = output.file.response.responseObj;
        this.showFileUploadSuccess = true;
        let element: HTMLElement = document.getElementById('cancelButton') as HTMLElement;
        element.click();
        this.messageService.sendMessage('reloadFromPE');   
      }
      else {
        this.isLoading = false;
      }
    }
  }

  disableSubmit() {
    if (this.showWrongExtError || !this.comments || !this.disputeTypes) {
      return true;
    }
    else {
      return false;
    }
  }

  getPickList() {
    this.deviceInTransitService.getDisputeDetails().subscribe(
      (res) => {

        this.pickList = res;
      },
      (err) => {

      }
    )
  }

  ngOnInit() {
    this.getPickList();
  }
}