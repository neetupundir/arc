import { Component, Inject, OnInit, ViewChild, ElementRef, Renderer2, Output, EventEmitter, } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddSparePartComponent } from '../add-spare-part/add-spare-part.component';
import { PendingEstimationService } from '../service/pending-estimation.service';
import { DeviceInTransitService } from '../service/device-in-transit.service';
import { MessageService } from '../service/message.service';
@Component({
  selector: 'app-recieved-device-reestimation',
  templateUrl: './recieved-device-reestimation.component.html',
  styleUrls: ['./recieved-device-reestimation.component.scss']
})
export class RecievedDeviceReEstimationComponent implements OnInit {
  uploadInput: any;
  options: any = {};
  BOMResponse: any;
  taxVal: number = 0;
  totalAmount: number = 0;
  serviceCharge: any;
  taxPercentage: any;
  addRecord: boolean = false;
  newPartName: string;
  newDescription: string;
  newPrice: any;
  newTaxes: any;
  newComment: string;
  addArray = [];
  newPartCode: string;
  submitBOMObj = {}
  res: any;
  comment: any;
  estimateArray: any = [];
  submitEstimateArray = [];
  taxGroup: any;
  countryId: any;
  modelId: any;
  brandId: any;
  taxgrpId: any;
  bomSubmitRes: any;
  bomRessMessage: any;
  isLoading: boolean = false;
  selectedArray = [];
  itemExist: boolean = true;
  amountCalculated: boolean = false;
  getReEstimationDetailsRes: any;
  approvedAmount: number = 0;
  orderEstimateId: any;
  BOMHistory:any;
  /**Declearation for show hide panel content */
  isToggle: boolean = false;

  @ViewChild('scrollContainer') private myScrollContainer: ElementRef;
  constructor(
    public dialog: MatDialog, private _renderer: Renderer2,private messageService: MessageService,
    public dialogRef: MatDialogRef<RecievedDeviceReEstimationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private deviceInTransitService: DeviceInTransitService, private pendingEstimationService: PendingEstimationService
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  cancel() {
    this.addRecord = false;
    this.newPartName = '';
    this.newDescription = '';
    this.newPrice = '';
    this.newTaxes = '';
    this.newComment = '';
    this.newPartCode = '';
  }
  files = [
    { value: 'steak-0', viewValue: 'Bottom Image' },
    { value: 'pizza-1', viewValue: 'Front Image' },
    { value: 'tacos-2', viewValue: 'Back Image' },
    { value: 'tacos-2', viewValue: 'Left Image' },
    { value: 'tacos-2', viewValue: 'Right Image' }
  ];
  /**
   * Add spare part & price
   */
  addSharePart(): void {
    let dialogRef = this.dialog.open(AddSparePartComponent, {
      width: '280px',
      disableClose: true,
      data: { name: 'test' }
    });
  }

  onlyNumber(e) {

    if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return true;
    } else if (!(e.keyCode > 47 && e.keyCode < 58) || e.shiftKey) {
      e.preventDefault();
      return false;
    }
  }
  calPerValue(val, per) {
    var value = parseFloat(val);
    var percentage = parseFloat(per);
    return (value * percentage) / 100;
  }

  getBOM() {
    var obj = {
      "countryId": 1,
      "brandId": 1,
      "modelId": "1"
    }

    this.isLoading = true;
    this.pendingEstimationService.getBOM(obj).subscribe(
      (res) => {
        this.isLoading = false;
        this.BOMResponse = res;
        this.countryId = this.BOMResponse.response.bomTemplate[0].countryId;
        this.modelId = this.BOMResponse.response.bomTemplate[0].modelId;
        this.brandId = this.BOMResponse.response.bomTemplate[0].brandId;

      },
      (err) => {

      }
    )
  }

  getReEstimationDetails() {
    let orderId = this.dialogRef._containerInstance._config.data.orderId;
    let obj = {
      "orderId": orderId
    }
    this.deviceInTransitService.getReEstimationDetails(obj).subscribe(
      (res) => {
        this.getReEstimationDetailsRes = res;
        this.orderEstimateId = this.getReEstimationDetailsRes.response.estimation.orderEstimateId;
        for (let i = 0; i < this.getReEstimationDetailsRes.response.estimation.orderEstimateDtls.length; i++) {
          this.approvedAmount += parseFloat(this.getReEstimationDetailsRes.response.estimation.orderEstimateDtls[i].price) + parseFloat(this.getReEstimationDetailsRes.response.estimation.orderEstimateDtls[i].partTaxValue);
        }
        this.getBOM();
      },
      (err) => {

      }
    )
  }

  add() {
    let obj = {
      "countryId": this.countryId,
      "modelId": this.modelId,
      "brandId": this.brandId,
      'partName': this.newPartName,
      'partDesc': this.newDescription,
      'partCode': this.newPartCode,
      'price': this.newPrice,
      "taxGrpId": this.newTaxes
    }
    this.pendingEstimationService.addDevicesForAMS(obj).subscribe(
      (res) => {
        this.addRecord = false;
        this.getBOM();
        this.cancel();
      },
      (err) => {

      }
    )
  }

  enableAdd() {
    if (!this.newPartName || !this.newPartCode || !this.newDescription || !this.newPrice || typeof (this.newTaxes) == 'undefined') {
      return true
    }
    else {
      return false;
    }
  }

  @Output()
  changeExpansion: EventEmitter<boolean> = new EventEmitter<boolean>();

  toggle() {
    this.isToggle = !this.isToggle;
    this.changeExpansion.emit(this.isToggle);
  }

  calAmountEstimation(serCharge, taxPercentage) {
    if (serCharge && taxPercentage) {
      let taxVal = (parseFloat(serCharge) * parseFloat(taxPercentage)) / 100;
      return taxVal;
    }
  }

  ngOnInit() {
    this.getReEstimationDetails();
    this.getRepairOrderDetails();
    this.getTaxGroup();
    this.getBOMHistory();
  }

  getTaxGroup() {
    let obj = {
      "countryId": 1
    }
    this.pendingEstimationService.getTaxGroup(obj).subscribe(
      (res) => {

        this.taxGroup = res;
      },
      (err) => {

      }
    )
  }
  getRepairOrderDetails() {
    this.isLoading = true;
    var obj = {
      "orderId": this.dialogRef._containerInstance._config.data.orderId
    }

    this.deviceInTransitService.getRODetails(obj).subscribe(
      (res) => {

        this.isLoading = false;
        this.res = res;
      },
      (err) => {

      }
    )
  }

  getBOMHistory() {
    // let orderId = this.dialogRef._containerInstance._config.data.orderId;
    // let obj = {
    //   "orderId": orderId
    // }
    var obj = {
      "orderId": 65
    }
    this.pendingEstimationService.getBOMHistory(obj).subscribe(
      (res) => {
      this.BOMHistory=res.response.bomHistory;
      },
      (err) => {

      }
    )
  }


  submitBOM() {

    this.isLoading = true;
    this.initializeEstimateArray();
    for (let i = 0; i < this.taxGroup.responseObj.length; i++) {
      if (this.taxGroup.responseObj[i].rateApplicable == this.taxPercentage) {
        this.taxgrpId = this.taxGroup.responseObj[i].taxGroupId;
        break;
      }
    }
    this.submitBOMObj = {
      "orderEstimateId": this.orderEstimateId,
      "orderId": this.dialogRef._containerInstance._config.data.orderId,
      "totalAmt": this.totalAmount,
      "approvedComments": this.comment,
      "action": "Add New Estimate",
      "status": true,
      "serviceCharge": this.serviceCharge,
      "taxGrpId": this.taxgrpId,
      "taxValue": this.taxVal,
      "orderEstimateDtls": this.initializeEstimateArray()
    }
    this.pendingEstimationService.saveReEstimation(this.submitBOMObj).subscribe(
      (res) => {
        this.bomSubmitRes = res;
        this.bomRessMessage = this.bomSubmitRes.response.Estimation;
        this.isLoading = false;   
        this.onNoClick();
        this.messageService.sendMessage('reEstimation');  
      },
      (err) => {
        let element: HTMLElement = document.getElementById('cancelButton') as HTMLElement;
        element.click();
        this.messageService.sendMessage('reEstimation');  
      }
    )
  }

  initializeEstimateArray() {
    this.estimateArray = [];
    for (let i = 0; i < this.BOMResponse.response.bomTemplate.length; i++) {
      if (this.BOMResponse.response.bomTemplate[i].checked) {
        this.estimateArray.push(
          {
            "partName": this.BOMResponse.response.bomTemplate[i].partName,
            "partId": this.BOMResponse.response.bomTemplate[i].partId,
            "partCode": this.BOMResponse.response.bomTemplate[i].partCode,
            "description": this.BOMResponse.response.bomTemplate[i].partDesc,
            "comments": this.BOMResponse.response.bomTemplate[i].comments,
            "partTaxValue": this.BOMResponse.response.bomTemplate[i].taxValue,
            "partTaxAmt": this.calAmountEstimation(this.BOMResponse.response.bomTemplate[i].price, this.BOMResponse.response.bomTemplate[i].taxValue),
            "approvalStatus": 2,
            "approvalComments": "Approved by ARC",
            "price": this.BOMResponse.response.bomTemplate[i].price
          }
        );
      }
    }
    return this.estimateArray;
  }



  calculateTaxAndAmount() {
    this.taxVal = 0;
    this.totalAmount = 0;
    for (var i = 0; i < this.BOMResponse.response.bomTemplate.length; i++) {
      if (this.BOMResponse.response.bomTemplate[i].checked) {
        var taxVal = (parseFloat(this.BOMResponse.response.bomTemplate[i].price) * parseFloat(this.BOMResponse.response.bomTemplate[i].taxValue)) / 100;
        this.taxVal += taxVal;
        this.totalAmount += parseFloat(this.BOMResponse.response.bomTemplate[i].price);
        this.BOMResponse.response.bomTemplate[i].taxamt = taxVal;
        this.amountCalculated = true;
      }
    }
    this.totalAmount = this.totalAmount + this.approvedAmount;

    if (typeof this.serviceCharge == 'undefined') {
      this.serviceCharge = 0;
    }
    if (typeof this.serviceCharge != 'undefined' && typeof this.taxPercentage == 'undefined') {
      this.taxPercentage = 0;
    }
    this.taxVal += (parseFloat(this.serviceCharge) * parseFloat(this.taxPercentage)) / 100;
    this.taxVal = parseFloat(this.taxVal.toFixed(2));
    this.totalAmount += parseFloat(this.serviceCharge) + this.taxVal;
    this.totalAmount = parseFloat(this.totalAmount.toFixed(2));
  }

  calculateValues(item) {
    let value = (item.price * item.taxValue) / 100;
    var valueParsed = parseFloat(value.toFixed(2));
    return valueParsed;
  }

  onUploadOutput(event) { }
}

