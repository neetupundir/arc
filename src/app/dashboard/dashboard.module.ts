import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { DashboardRoutingModule } from "./dashboard.routing.module";
import { SharedModule } from "../shared/shared.module";
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSortModule } from '@angular/material/sort';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatProgressSpinnerModule, MatCheckboxModule, MatCardModule, MatFormFieldModule, MatInputModule, MatIconModule, MatDialogModule, MatMenuModule, MatTabsModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatRadioModule } from '@angular/material';
import { NgUploaderModule } from 'ngx-uploader';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DeviceInTransitComponent } from './device-in-transit/device-in-transit.component';
import { RepairOrderDetailsComponent } from './repair-order-details/repair-order-details.component';
import { RecievedDeviceComponent } from './recieved-device/recieved-device.component';
import { PendingForApprovalComponent } from './pending-for-approval/pending-for-approval.component';
import { RepairInProgressComponent } from './repair-in-progress/repair-in-progress.component';
import { PendingForDispatchComponent } from './pending-for-dispatch/pending-for-dispatch.component';
import { DeviceDispatchedComponent } from './device-dispatched/device-dispatched.component';
import { DeviceInTransitRecievedComponent } from './device-in-transit-recieved/device-in-transit-recieved.component';
import { DeviceInTransitDisputeComponent } from './device-in-transit-dispute/device-in-transit-dispute.component';
import { RecievedDeviceEstimationComponent } from './recieved-device-estimation/recieved-device-estimation.component';
import { RecievedDeviceReEstimationComponent } from './recieved-device-reestimation/recieved-device-reestimation.component';
import { AddSparePartComponent } from './add-spare-part/add-spare-part.component';
import { PendingForApprovalAmountComponent } from './pending-for-approval-amount/pending-for-approval-amount.component';
import { PendingForDispatchDetailComponent } from './pending-for-dispatch-detail/pending-for-dispatch-detail.component';
import { RepairInProgressDoneComponent } from './repair-in-progress-done/repair-in-progress-done.component';
import { FormsModule } from '@angular/forms';
import { SortService } from './service/table-sort.service';
import { SortableTableDirective } from './service/sort-directive';
import { ZoomDeviceInTransitComponent } from './zoom-device-in-transit/zoom-device-in-transit.component';
import { ModifyRepairDateComponent } from './modify-repair-date/modify-repair-date.component';
import { DeviceInTransitService } from './service/device-in-transit.service';
import {PendingEstimationService} from './service/pending-estimation.service';
import {RepairInProgressService } from './service/repair-in-progress.service';
import {PendingForAMSService} from './service/pending-for-AMS.service';
import {PendingForDispatchService} from './service/pending-for-dispatch.service';
import {DeviceDispatchedService} from './service/device-dispatched.service';
import { DataTablesModule } from 'angular-datatables';
import { ZoomPendingEstimationComponent } from './zoom-pending-estimation/zoom-pending-estimation.component';
import { ZoomPendingForApprovalComponent } from './zoom-pending-for-approval/zoom-pending-for-approval.component';
import { ZoomPendingForDispatchComponent } from './zoom-pending-for-dispatch/zoom-pending-for-dispatch.component';
import { ZoomRepairInProgressComponent } from './zoom-repair-in-progress/zoom-repair-in-progress.component';
import { ZoomDeviceDispatchedComponent } from './zoom-device-dispatched/zoom-device-dispatched.component';
import {MessageService} from './service/message.service';
import { PlannedDateHistoryComponent } from './planned-date-history/planned-date-history.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DashboardRoutingModule,
    SharedModule,
    MatButtonModule,
    MatRadioModule,
    MatCheckboxModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSidenavModule,
    MatSortModule,
    MatDialogModule,
    MatMenuModule,
    MatTabsModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FlexLayoutModule,
    NgUploaderModule,
    FormsModule,
    DataTablesModule
  ],
  entryComponents: [
    DashboardComponent,
    RepairOrderDetailsComponent,
    DeviceInTransitRecievedComponent,
    DeviceInTransitDisputeComponent,
    RecievedDeviceEstimationComponent,
    RecievedDeviceReEstimationComponent,
    AddSparePartComponent,
    PendingForApprovalAmountComponent,
    PendingForDispatchDetailComponent,
    RepairInProgressDoneComponent,
    ZoomDeviceInTransitComponent,
    ModifyRepairDateComponent,
    ZoomPendingEstimationComponent,
    ZoomPendingForApprovalComponent,
    ZoomPendingForDispatchComponent,
    ZoomRepairInProgressComponent,
    ZoomDeviceDispatchedComponent,
    PlannedDateHistoryComponent
  ],
  declarations: [
    DashboardComponent,
    DeviceInTransitComponent,
    RepairOrderDetailsComponent,
    RecievedDeviceComponent,
    PendingForApprovalComponent,
    RepairInProgressComponent,
    PendingForDispatchComponent,
    DeviceDispatchedComponent,
    DeviceInTransitRecievedComponent,
    DeviceInTransitDisputeComponent,
    RecievedDeviceEstimationComponent,
    RecievedDeviceReEstimationComponent,
    AddSparePartComponent,
    PendingForApprovalAmountComponent,
    PendingForDispatchDetailComponent,
    RepairInProgressDoneComponent,
    SortableTableDirective,
    ZoomDeviceInTransitComponent,
    ModifyRepairDateComponent,
    ZoomPendingEstimationComponent,
    ZoomPendingForApprovalComponent,
    ZoomPendingForDispatchComponent,
    ZoomRepairInProgressComponent,
    ZoomDeviceDispatchedComponent,
    PlannedDateHistoryComponent
  ],
  providers: [
    MessageService,
    SortService,
    DeviceInTransitService,
    PendingEstimationService,
    RepairInProgressService,
    PendingForAMSService,
    PendingForDispatchService,
    DeviceDispatchedService
  ]
})
export class DashboardModule { }