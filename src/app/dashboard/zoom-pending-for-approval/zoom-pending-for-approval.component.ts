import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { PendingForAMSService } from '../service/pending-for-AMS.service';
import { Subject } from 'rxjs';
import { RepairOrderDetailsComponent } from '../repair-order-details/repair-order-details.component';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

@Component({
  selector: 'app-zoom-pending-for-approval',
  templateUrl: './zoom-pending-for-approval.component.html',
  styleUrls: ['./zoom-pending-for-approval.component.scss']
})
export class ZoomPendingForApprovalComponent{

  uploadInput : any;
	options : any = {};
  deviceResponse:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  isTableData:boolean=false;
  
 	constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ZoomPendingForApprovalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private pendingForAMSService:PendingForAMSService
  ) { }

  getPendingForApproval() {
    let currentUser=localStorage.getItem('currentUser');
    var obj = {
      "arcId": JSON.parse(currentUser).arcId,
      "country": "India",
      "orderState": "Pending for Approval",
      "isDetailView":true
    }
    this.pendingForAMSService.getPendingForAMS(obj).subscribe(
      (res) => {
      
        this.deviceResponse = res.response.roList;
        this.dtTrigger.next();
        this.isTableData = true;
      },
      (err) => {

      }
    )
  }

  ngAfterViewInit() {
    this.dtTrigger.next();
 }

 ngOnInit() {
  this.getPendingForApproval();
}

  onNoClick(): void {
    this.dialogRef.close();
  }

  downloadData(): void{
    let responseArr = this.deviceResponse, internalArr = [],externalArr = [];

    for( let result of responseArr ){
      internalArr=[];
      internalArr.push(result.brand);
      internalArr.push("-"+result.imeiNo+"-");
      internalArr.push(result.estimationDate);
      internalArr.push(result.estimatedBy);
      internalArr.push(result.amount);
      internalArr.push(result.approvalDueDate);
      externalArr.push(internalArr);
    }
    var options = { 
      headers: ['Brand Name','IMEI No.','Estimation Date','Estimation By','Amount','Approval Due Date/Time']
      };
      new Angular2Csv(externalArr, 'Pending for Approval Report',options);
      
     
}
getRepairOrderDetails(orderId): void {
  let dialogRef = this.dialog.open(RepairOrderDetailsComponent, {
    width: '500px',
    data: { orderID: orderId }
  });
}

}
