import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomPendingForApprovalComponent } from './zoom-pending-for-approval.component';

describe('ZoomPendingForApprovalComponent', () => {
  let component: ZoomPendingForApprovalComponent;
  let fixture: ComponentFixture<ZoomPendingForApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoomPendingForApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomPendingForApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
