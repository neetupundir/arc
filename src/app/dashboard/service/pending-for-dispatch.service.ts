import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient,HttpHeaders } from "@angular/common/http";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { environment } from '../../../environments/environment';

@Injectable()
export class PendingForDispatchService {

    baseUrl: string = environment.hostUrl;
    // baseUrl: string = "http://192.168.3.148:8020";
    constructor(private _http: HttpClient) {
    }

    getPendingForDispatch(obj): Observable<any> {  
       
        return this._http.post(this.baseUrl + '/repairPortal/getPendingForDispatch',obj)
    }

    getCourierName(obj):Observable<any>{
        return this._http.post(this.baseUrl+'/picklist/getLSBList',obj)
    }

}
