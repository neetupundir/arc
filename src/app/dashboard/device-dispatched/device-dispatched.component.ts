import { Component, OnInit,Inject, Input, Output, EventEmitter, trigger, style, transition, animate } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DeviceDispatchedService } from '../service/device-dispatched.service';
import { ZoomDeviceDispatchedComponent } from '../zoom-device-dispatched/zoom-device-dispatched.component';
@Component({
  selector: 'app-device-dispatched',
  templateUrl: './device-dispatched.component.html',
  styleUrls: ['./device-dispatched.component.scss'],
  animations: [
    trigger(
      'isToggle', [
        transition(':enter', [
          style({ 'height': '0', 'opacity': '0' }),
          animate('200ms linear', style({ 'height': '*', 'opacity': '1' }))
        ]),
        transition(':leave', [
          style({ 'height': '*', 'opacity': '1' }),
          animate('200ms linear', style({ 'height': '0', 'opacity': '0' })
          )])
      ]
    )]
})
export class DeviceDispatchedComponent implements OnInit {

  /*declation of input parameter for dashboard items*/
  @Input() public show: boolean;

  @Input() public set changeInPendingForDispatch(val: any) {
		if (typeof val != "undefined") {
      debugger;
			this.ngOnInit();
		}
	}


  @Output()
  changeExpansion: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**Declearation for show hide panel content */
  isToggle: boolean = true;
  res:any;

  // data = [
  //   { awbNumber: '12345', courier: 'DTDC', customer: 'ABC', dispatchedDate: '2017-11-17T08:34:32', deliveryStaus: 'Delivered' },
  //   { awbNumber: '12244', courier: 'DHL', customer: 'XYZ', dispatchedDate: '2018-02-01T08:34:32', deliveryStaus: 'Not Delivered' },
  //   // { awbNumber: '12244',courier: 'DHL',customer: 'XYZ', dispatchedDate: '2018-02-01T08:34:32', deliveryStaus: 'Not Delivered' },
  //   // { awbNumber: '12244',courier: 'DHL',customer: 'XYZ', dispatchedDate: '2018-02-01T08:34:32', deliveryStaus: 'Not Delivered' },
  //   // { awbNumber: '12244',courier: 'DHL',customer: 'XYZ', dispatchedDate: '2018-02-01T08:34:32', deliveryStaus: 'Not Delivered' },
  //   // { awbNumber: '12244',courier: 'DHL',customer: 'XYZ', dispatchedDate: '2018-02-01T08:34:32', deliveryStaus: 'Not Delivered' },
  // ];

  constructor(private deviceDispatchedService: DeviceDispatchedService,public dialog: MatDialog) { }

  ngOnInit() {
    this.getDeviceDispatched();
  }

  getDeviceDispatched() {
    let currentUser=localStorage.getItem('currentUser');
    var obj = {
      "arcId": JSON.parse(currentUser).arcId,
      "orderState": "Device Dispatched",
      "country": "India"
    }
    this.deviceDispatchedService.getDeviceDispatched(obj).subscribe(
      (res) => {
       
        this.res = res;
        this.isToggle = !this.isToggle;
        this.changeExpansion.emit(this.isToggle);
        setTimeout(() => {    //<<<---    using ()=> syntax
          this.isToggle = true;
          this.changeExpansion.emit(this.isToggle);
        }, 1000);
      },
      (err) => {

      }
    )
  }
  /**
  *Method for toggle show hide
  */
  toggle() {
    this.isToggle = !this.isToggle;
    this.changeExpansion.emit(this.isToggle);
  }

  getIsDate(date) {
    if (new Date(date) < new Date()) {
      return true;
    }
  }

  ZoomTransit():void{
    let dialogRef = this.dialog.open(ZoomDeviceDispatchedComponent, {
      width: '1100px',
      disableClose: true,
      data: { name: 'test' }
    });
  }

}
