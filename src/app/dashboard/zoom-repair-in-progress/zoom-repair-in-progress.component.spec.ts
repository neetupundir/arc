import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomRepairInProgressComponent } from './zoom-repair-in-progress.component';

describe('ZoomRepairInProgressComponent', () => {
  let component: ZoomRepairInProgressComponent;
  let fixture: ComponentFixture<ZoomRepairInProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoomRepairInProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomRepairInProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
