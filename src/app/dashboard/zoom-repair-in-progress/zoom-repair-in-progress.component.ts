import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { RepairInProgressService } from '../service/repair-in-progress.service';
import { Subject } from 'rxjs';
import { RepairOrderDetailsComponent } from '../repair-order-details/repair-order-details.component';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {RepairInProgressDoneComponent} from '../repair-in-progress-done/repair-in-progress-done.component';
import {RecievedDeviceReEstimationComponent} from '../recieved-device-reestimation/recieved-device-reestimation.component';
@Component({
  selector: 'app-zoom-repair-in-progress',
  templateUrl: './zoom-repair-in-progress.component.html',
  styleUrls: ['./zoom-repair-in-progress.component.scss']
})
export class ZoomRepairInProgressComponent {

  uploadInput : any;
  options : any = {};
  deviceResponse:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  isTableData:boolean=false;
  repairInProgressRes:any;
 	constructor(
    public dialog: MatDialog,
	  public dialogRef: MatDialogRef<ZoomRepairInProgressComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private repairInProgressService:RepairInProgressService
  ) { }

  getRepairInProgess() {
    let currentUser=localStorage.getItem('currentUser');
    var obj = {
      "arcId": JSON.parse(currentUser).arcId,
      "orderState": "Repair in Progress",
      "country": "India",
      "isDetailView":true
    }
    this.repairInProgressService.getRepairInProgress(obj).subscribe(
      (res) => {
        this.repairInProgressRes = res;
        this.dtTrigger.next();
        this.isTableData = true;
      },
      (err) => {

      }
    )
  }

  ngAfterViewInit() {
    this.dtTrigger.next();
 }

 ngOnInit() {
  this.getRepairInProgess();
}

  onNoClick(): void {
    this.dialogRef.close();
  }
  downloadData(): void{
    let responseArr = this.repairInProgressRes, internalArr = [],externalArr = [];
  debugger;
    for(let result of responseArr.response.roList){
      internalArr=[];
      internalArr.push(result.brand + " " + result.model);
      internalArr.push(result.imeiNo);
      internalArr.push(result.estimationApprovedDate);
      internalArr.push(result.repairDueDate);
      internalArr.push(result.repairPlannedDate);
      externalArr.push(internalArr);
    }
    var options = { 
      headers: ['Brand Name','IMEI No.','Estimation Approved Date','Repair Due Date','Repair Planned Date']
      };
      new Angular2Csv(externalArr, 'Repair in Pregress Report',options);    
      
  }
    /**
  *Done
  */
 done(id): void {
  let dialogRef = this.dialog.open(RepairInProgressDoneComponent, {
    width: '500px',
    //disableClose:true,
    data: { orderId: id }
  });
}
/**
*reEstimation
*/
reEstimation(orderId): void {
  let dialogRef = this.dialog.open(RecievedDeviceReEstimationComponent, {
    width: '700px',
    //disableClose:true,
    data: { orderId: orderId }
  });
}

getRepairOrderDetails(orderId): void {
  let dialogRef = this.dialog.open(RepairOrderDetailsComponent, {
    width: '500px',
    data: { orderID: orderId }
  });
}
}
