import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {DeviceInTransitService} from '../service/device-in-transit.service';
import { RepairOrderDetailsComponent } from '../repair-order-details/repair-order-details.component'
import { Subject } from 'rxjs';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
@Component({
  selector: 'app-zoom-device-in-transit',
  templateUrl: './zoom-device-in-transit.component.html',
  styleUrls: ['./zoom-device-in-transit.component.scss']
})
export class ZoomDeviceInTransitComponent {

  uploadInput : any;
  options : any = {};
  deviceResponse:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  isTableData:boolean=false;
  
 	constructor(
    public dialog: MatDialog,
	  public dialogRef: MatDialogRef<ZoomDeviceInTransitComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private deviceInTransitService:DeviceInTransitService
  ) { }

  getDevicesInTransit() {
    let currentUser=localStorage.getItem('currentUser');
    var obj = {
      "arcId": JSON.parse(currentUser).arcId,
      "country": "India",
      "orderState": "Device in Transit",
      "isDetailView":true
    }
    this.deviceInTransitService.getDeviceInTransit(obj).subscribe(
      (res) => {
        
        this.deviceResponse = res.response.roList;
        this.dtTrigger.next();
        this.isTableData = true;
      },
      (err) => {

      }
    )
    
  }
  getRepairOrderDetails(orderId): void {
    let dialogRef = this.dialog.open(RepairOrderDetailsComponent, {
      width: '500px',
      data: { orderID: orderId }
    });
  }

  ngAfterViewInit() {
    this.dtTrigger.next();
 }

 ngOnInit() {
  this.getDevicesInTransit();
}

  onNoClick(): void {
    this.dialogRef.close();
  }
  downloadData(): void{
    let responseArr = this.deviceResponse, internalArr = [],externalArr = [];
      

    for(let result of responseArr){
      internalArr=[];
      internalArr.push(result.brand);
      internalArr.push(result.model);
      internalArr.push("-"+result.imeiNo+"-");
      internalArr.push(result.eta);
      internalArr.push(result.awbNo);
      internalArr.push(result.courierName);
      externalArr.push(internalArr);
    }
    var options = { 
      headers: ['Brand Name','Model Name','IMEI No.','Estimation','AWB No.','Courier Name']
      };
      new Angular2Csv(externalArr, 'Device in Transit Report',options);    
      
  }
}

