import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomDeviceInTransitComponent } from './zoom-device-in-transit.component';

describe('ZoomDeviceInTransitComponent', () => {
  let component: ZoomDeviceInTransitComponent;
  let fixture: ComponentFixture<ZoomDeviceInTransitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoomDeviceInTransitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomDeviceInTransitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
