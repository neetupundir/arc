import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { PendingForDispatchService } from '../service/pending-for-dispatch.service';
import { Subject } from 'rxjs';
import { RepairOrderDetailsComponent } from '../repair-order-details/repair-order-details.component';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

@Component({
  selector: 'app-zoom-pending-for-dispatch',
  templateUrl: './zoom-pending-for-dispatch.component.html',
  styleUrls: ['./zoom-pending-for-dispatch.component.scss']
})
export class ZoomPendingForDispatchComponent {

  uploadInput : any;
  options : any = {};
  deviceResponse:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  isTableData:boolean=false;
  
 	constructor(
    public dialog: MatDialog,
	  public dialogRef: MatDialogRef<ZoomPendingForDispatchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private pendingForDispatchService:PendingForDispatchService
  ) { }

  getDevicesInTransit() {
    let currentUser=localStorage.getItem('currentUser');
    var obj = {
      "arcId": JSON.parse(currentUser).arcId,
      "country": "India",
      "orderState": "Pending for Dispatch",
      "isDetailView":true
    }
    this.pendingForDispatchService.getPendingForDispatch(obj).subscribe(
      (res) => {
  
        this.deviceResponse = res.response.roList;
        this.dtTrigger.next();
        this.isTableData = true;
      },
      (err) => {

      }
    )
    
  }

  ngAfterViewInit() {
    this.dtTrigger.next();
 }

 ngOnInit() {
  this.getDevicesInTransit();
}

  onNoClick(): void {
    this.dialogRef.close();
  }
  downloadData(): void{
    let responseArr = this.deviceResponse, internalArr = [],externalArr = [];
      

    for(let result of responseArr){
      internalArr=[];
      internalArr.push(result.brand);
      internalArr.push("-"+result.imeiNo+"-");
      internalArr.push(result.status);
      
    }
    var options = { 
      headers: ['Brand Name','IMEI No.','Status']
      };
      new Angular2Csv(externalArr, 'Pending for Dispatch Report',options);    
      
  }
  getRepairOrderDetails(orderId): void {
    let dialogRef = this.dialog.open(RepairOrderDetailsComponent, {
      width: '500px',
      data: { orderID: orderId }
    });
  }

}
