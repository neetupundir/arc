import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomPendingForDispatchComponent } from './zoom-pending-for-dispatch.component';

describe('ZoomPendingForDispatchComponent', () => {
  let component: ZoomPendingForDispatchComponent;
  let fixture: ComponentFixture<ZoomPendingForDispatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoomPendingForDispatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomPendingForDispatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
