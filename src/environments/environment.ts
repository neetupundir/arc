// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export interface EnvironmentSettings {
  production: boolean;
  hostUrl: string;
  hostUrlRGAuth: string;
}

export const environment = {
  production: false, 
  hostUrl: 'http://fom-core-uat-arc.ap-southeast-1.elasticbeanstalk.com:8020',
  loginUrl:'http://13.250.110.97:8050',
  analyticHostUrl:'http://fom-core-uat-analytics.ap-southeast-1.elasticbeanstalk.com:8100'
};